//
//  MiamiModel.swift
//  EasyWeather
//
//  Created by JAIRO PROAÑO on 30/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import Foundation


struct MiamiWeather: Decodable {
    let id: Int
    let description: String
}


// Cuando se cambia nombres del json
/*
 enum CodingKeys: String, CodingKey {
 case wather
 case mainWeather = "main_weather"
 }
 */
struct MiamiModel: Decodable {
    let weather:[MiamiWeather]
    
}
