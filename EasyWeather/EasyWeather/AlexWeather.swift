//
//  AlexWeather.swift
//  EasyWeather
//
//  Created by alexolmedo on 30/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import Foundation

struct AlexWeatherInfo: Decodable {
    let weather: [AlexWeather]
    
}

struct AlexWeather: Decodable {
    let id:Int
    let description:String
}
