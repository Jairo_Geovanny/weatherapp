//
//  ViewControllerSeoul.swift
//  EasyWeather
//
//  Created by alexolmedo on 29/5/18.
//  Copyright © 2018 Alexander Olmedo. All rights reserved.
//

import UIKit

class AlexViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text?.replacingOccurrences(of: " ", with: "+") ?? "quito")&appid=ddbeb76a31c540f8f022b61f8732a38c"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response , error ) in
            
            guard let data = data else {
                print("Error, NO data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(AlexWeatherInfo.self, from:data) else {
                print("Error decoding Weather")
                return
            }
            
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            }
            
            print(weatherInfo.weather[0].description)
            
        }
        task.resume()
        
    }
    
}
